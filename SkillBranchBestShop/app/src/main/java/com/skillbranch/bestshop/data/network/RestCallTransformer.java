package com.skillbranch.bestshop.data.network;

import android.util.Log;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import com.skillbranch.bestshop.data.managers.DataManager;
import com.skillbranch.bestshop.data.network.error.ErrorUtils;
import com.skillbranch.bestshop.data.network.error.NetworkAvailableError;
import com.skillbranch.bestshop.utils.ConstantManager;
import com.skillbranch.bestshop.utils.NetworkStatusChecker;
import retrofit2.Response;
import rx.Observable;

import static com.skillbranch.bestshop.data.managers.DataManager.TAG;

public class RestCallTransformer<R> implements Observable.Transformer<Response<R>, R> {
    @Override
    @RxLogObservable
    public Observable<R> call(Observable<Response<R>> responseObservable) {
       return NetworkStatusChecker.isInternetAvialable()
                .flatMap(aBoolean -> aBoolean ? responseObservable : Observable.error(new NetworkAvailableError()))
                .flatMap(rResponse -> {
                    Log.e(TAG, "callRest: "+rResponse.code());
                    switch (rResponse.code()) {
                        case 200:
                            String lastModified = rResponse.headers().get(ConstantManager.LAST_MODIFIED_HEADER);
                            if(lastModified != null) {
                                DataManager.getInstance().getPreferencesManager().saveLastProductUpdate(lastModified);
                            }
                            return Observable.just(rResponse.body());
                        case 304:
                            return Observable.empty();
                        default:
                            return Observable.error(ErrorUtils.parseError(rResponse));
                    }
                });
    }
}
