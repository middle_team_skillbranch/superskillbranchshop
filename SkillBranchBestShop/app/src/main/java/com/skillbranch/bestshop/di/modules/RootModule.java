package com.skillbranch.bestshop.di.modules;

import dagger.Provides;
import com.skillbranch.bestshop.di.scopes.RootScope;
import com.skillbranch.bestshop.mvp.models.AccountModel;
import com.skillbranch.bestshop.mvp.models.RootModel;
import com.skillbranch.bestshop.mvp.presenters.RootPresenter;

@dagger.Module
public class RootModule {
    @Provides
    @RootScope
    RootModel provideRootModel() {
        return new RootModel();
    }

    @Provides
    @RootScope
    AccountModel provideAccountModel() {
        return new AccountModel();
    }

    @Provides
    @RootScope
    RootPresenter provideRootPresenter() {
        return new RootPresenter();
    }
}
